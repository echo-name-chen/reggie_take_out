package com.chenguo.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.chenguo.reggie.common.BaseContext;
import com.chenguo.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    // 路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // 1、获取本次请求的URI
        String requestURI = request.getRequestURI();//   /backend/index.html
        log.info("拦截到请求：{}", requestURI);

        // 定义不需要处理的请求路径
        String[] urls = new String[] {
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg", // 移动端发送短信
                "/user/login", // 移动端登陆
                // 放行接口文档路径
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };

        // 2、判断本次请求是否需要处理
        boolean check = check(urls, requestURI);

        // 3、如果不需要处理，则直接放行
        if (check) {
            log.info("本次请求{}不需要处理", requestURI);
            chain.doFilter(request, response);
            return;
        }
        // 4、判断登陆状态，如果已登陆，则直接放行
        if (request.getSession().getAttribute("employee") != null) {
            log.info("用户已登陆，用户id为：{}", request.getSession().getAttribute("employee"));
            // 存入ThreadLocal里面
            Long empId = (Long) request.getSession().getAttribute("employee");
            BaseContext.setCurrentId(empId);

            chain.doFilter(request, response);
            return;
        }

        // 4.2 判断登陆移动端状态，如果登陆了，放行
        if (request.getSession().getAttribute("user") != null) {
            log.info("用户已登陆，用户id为：{}", request.getSession().getAttribute("user"));
            // 存入ThreadLocal里面
            Long userId = (Long) request.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);

            chain.doFilter(request, response);
            return;
        }

        log.info("用户未登陆");
        // 5、如果未登陆则返回未登陆结果，通过输出流方式向客户端页面响应数据
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;

        // {} 这里是占位符
//        log.info("拦截到请求：{}", request.getRequestURI());
//        chain.doFilter(request, response);
    }
    /**
    * date: 2022/10/26 20:33
    * @author: Chen
    * @since JDK 1.8
    * Description:
     * 路径匹配，检查本次请求是否需要放行
    */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) {
                return true;
            }
        }
        return false;
    }
}
