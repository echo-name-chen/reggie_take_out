package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.User;

public interface UserService extends IService<User> {
}
