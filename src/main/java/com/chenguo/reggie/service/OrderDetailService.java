package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
