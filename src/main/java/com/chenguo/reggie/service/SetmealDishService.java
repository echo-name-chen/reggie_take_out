package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
