package com.chenguo.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chenguo.reggie.mapper.DishFlavorMapper;
import com.chenguo.reggie.pojo.DishFlavor;
import com.chenguo.reggie.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
    // 删除对应的菜品口味
    @Override
    public void removeDishFlavor(Long id) {
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(id != null, DishFlavor::getDishId, id);
        this.remove(queryWrapper);
    }
}
