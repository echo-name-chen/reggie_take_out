package com.chenguo.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chenguo.reggie.mapper.SetmealDishMapper;
import com.chenguo.reggie.pojo.SetmealDish;
import com.chenguo.reggie.service.SetmealDishService;
import org.springframework.stereotype.Service;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
