package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.Employee;

public interface EmployeeService extends IService<Employee> {
}
