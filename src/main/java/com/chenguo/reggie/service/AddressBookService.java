package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
}
