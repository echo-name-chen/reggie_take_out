package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.Category;

public interface CategoryService extends IService<Category> {
    void remove(Long id);
}
