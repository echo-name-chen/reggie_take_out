package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.dto.SetmealDto;
import com.chenguo.reggie.pojo.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {

    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐，同时删除套餐和菜品的关联关系
     * @param ids
     */
    void removeWithDish(List<Long> ids);

    /** 根据套餐id 修改售卖状态
     * @param status
     * @param ids
     */
    void updateSetmealStatusById(Integer status, List<Long> ids);

    /**
     * 根据id 查询套餐信息，返回给前端
     * @param id
     * @return
     */
    SetmealDto getSetmealAndDish(Long id);

    /**
     * 修改操作
     * @param setmealDto
     */
    void updateWithDish(SetmealDto setmealDto);
}
