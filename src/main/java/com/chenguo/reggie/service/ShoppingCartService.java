package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
    void clean();
}
