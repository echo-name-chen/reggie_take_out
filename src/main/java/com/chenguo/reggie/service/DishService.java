package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.dto.DishDto;
import com.chenguo.reggie.pojo.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {
    // 新增菜品，同时插入菜品对应的口味数据，需要同时操作两张表：dish，dish_flavor
    void saveWithFlavor(DishDto dishDto);

    // 根据id 查询菜品信息和对应的口味信息
    DishDto getByIdWithFlavor(Long id);

    // 更新菜品信息，同时更新对应的口味信息
    void updateWithFlavor(DishDto dishDto);

    // 删除菜品和关联的菜品口味
    void deleteByIds(List<Long> ids);
}
