package com.chenguo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.reggie.pojo.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
    // 删除对应的菜品口味
    void removeDishFlavor(Long id);
}
