package com.chenguo.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chenguo.reggie.common.R;
import com.chenguo.reggie.pojo.Employee;
import com.chenguo.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;


@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
    * date: 2022/10/26 17:19
    * @author: Chen
    * @since JDK 1.8
    * Description: 员工登陆
    */
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {
        /**
         * 1、将页面提交的密码password 进行一个md5 的加密处理
         * 2、根据页面提交的用户名username查询数据库
         * 3、如果没有查询到则返回登陆失败结果
         * 4、密码比对时，如果不一致则返回登陆失败结果
         * 5、查看员工状态，如果为已禁用状态，则返回员工已禁用
         * 6、登陆成功，将员工id 存入Session并返回登陆成功的结果
         */

        // 1、将页面提交的密码password 进行一个md5 的加密处理
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        // 2、根据页面提交的用户名username查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername, employee.getUsername());
        // 为什么这里调用的是getOne， 原因是我们在数据库中username 这个字段我们设置了unique索引
        Employee emp = employeeService.getOne(queryWrapper);

        // 3、如果没有查询到则返回登陆失败结果
        if (emp == null) {
            return R.error("登陆失败");
        }

        // 4、密码比对时，如果不一致则返回登陆失败结果
        if (!emp.getPassword().equals(password)) {
            return R.error("登陆失败");
        }

        // 5、查看员工状态，如果为已禁用状态，则返回员工已禁用
        if (emp.getStatus() == 0) {
            return R.error("账号已封禁");
        }

        // 6、登陆成功，将员工id 存入Session并返回登陆成功的结果
        request.getSession().setAttribute("employee", emp.getId());
        return R.success(emp);
    }

    /**
    * date: 2022/10/26 17:19
    * @author: Chen
    * @since JDK 1.8
    * Description: 员工退出
    */
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        // 1 清理Session 中保存的当前登陆的员工id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    /**
    * date: 2022/10/27 20:31
    * @author: Chen
    * @since JDK 1.8
    * Description:
     * 新增员工
    */
    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("新增员工，员工信息：{}", employee);
        // 设置初始密码123456，需要进行md5的加密处理
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

        /*employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());

        // 获取当前登陆用户的id
        Long empId = (Long) request.getSession().getAttribute("employee");

        employee.setCreateUser(empId);
        employee.setUpdateUser(empId);*/

        employeeService.save(employee);
        return R.success("新增员工成功");
    }

    /**
    * date: 2022/10/27 21:43
    * @author: Chen
    * @since JDK 1.8
    * Description:
     * 员工信息分页查询
    */
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {

        log.info("page = {}, pageSize = {}, name = {}", page, pageSize, name);

        // 构造分页构造器
        Page pageInfo = new Page(page, pageSize);

        // 构造条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        // 添加过滤条件
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);

        // 添加排序条件
        queryWrapper.orderByDesc(Employee::getUpdateTime);

        // 执行查询
        employeeService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

    /**
    * date: 2022/10/28 9:35
    * @author: Chen
    * @since JDK 1.8
    * Description:
     * 根据id修改员工信息
    */
    @PutMapping
    public R<String> update(HttpServletRequest request, @RequestBody Employee employee) {
        log.info(employee.toString());

        /*Long empId = (Long) request.getSession().getAttribute("employee");
        employee.setUpdateUser(empId);
        employee.setUpdateTime(LocalDateTime.now());*/
        employeeService.updateById(employee);

        return R.success("员工信息修改成功");
    }

    /**
     * 根据id查询员工信息
     * 回显数据到修改页面
     */

    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id) {
        log.info("根据id 查询员工信息...");
        Employee employee = employeeService.getById(id);
        if (employee != null) {
            return R.success(employee);
        }
        return R.error("没有查询到对应员工的信息");
    }


}
