package com.chenguo.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chenguo.reggie.pojo.OrderDetail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
