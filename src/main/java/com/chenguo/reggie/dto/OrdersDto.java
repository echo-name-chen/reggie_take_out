package com.chenguo.reggie.dto;


import com.chenguo.reggie.pojo.OrderDetail;
import com.chenguo.reggie.pojo.Orders;
import lombok.Data;
import lombok.extern.java.Log;

import java.util.List;

@Data
public class OrdersDto extends Orders {

    private String userName;

    private String phone;

    private String address;

    private String consignee;

    // 有几件商品
    private Long sumNum;

    private List<OrderDetail> orderDetails;
	
}
