package com.chenguo.reggie.common;

public class CustomException extends RuntimeException{
    public CustomException (String message) {
        super(message);
    }
}
