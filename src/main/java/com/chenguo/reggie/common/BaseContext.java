package com.chenguo.reggie.common;

/**
* date: 2022/10/28 17:16
* @author: Chen
* @since JDK 1.8
* Description:
 * 基于ThreadLocal封装工具类，用户保存和获取当前登陆用户id
 * 因为每个线程都是隔离的，所以存入的值不会混淆
*/

public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }

    public static Long getCurrentId() {
        return threadLocal.get();
    }
}
